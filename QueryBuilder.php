<?php

declare(strict_types=1);

final class QueryBuilder
{
    private const SELECT = 'SELECT %s';
    private const FROM = ' FROM %s';
    private const WHERE = ' WHERE %s';
    private const ORDER_BY = ' ORDER BY %s';
    private const AND = ' AND %s';
    private const OR = ' OR %s';

    private \PDOStatement $sth;
    private \PDO $connection;
    private string $select = '';
    private string $from = '';
    private string $where = '';
    private string $orderBy = '';

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function select(string $select): self
    {
        $this->select = $select;

        return $this;
    }

    public function addSelect(string $select): self
    {
        $this->select .= sprintf(', %s', $select);

        return $this;
    }

    public function from(string $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function andWhere(string $condition): self
    {
        if ('' === $this->where) {
            $this->where = $condition;
        } else {
            $this->where .= sprintf(self::AND, $condition);
        }

        return $this;
    }

    public function orWhere(string $condition): self
    {
        if ('' === $this->where) {
            $this->where = $condition;
        } else {
            $this->where .= sprintf(self::OR, $condition);
        }

        return $this;
    }

    public function addOrderBy(string $column, string $format = ' ASC'): self
    {
        if ('' === $this->orderBy) {
            $this->orderBy = sprintf('%s %s', $column, $format);
        } else {
            $this->orderBy .= sprintf(', %s %s', $column, $format);
        }

        return $this;
    }

    public function execute(): self
    {
        if ('' === $this->select) {
            throw new \InvalidArgumentException('Select is empty');
        }

        if ('' === $this->from) {
            throw new \InvalidArgumentException('From is empty');
        }

        $this->sth = $this->connection->query($this->buildQuery());

        return $this;

    }

    public function fetchAll(): array
    {
        return $this->sth->fetchAll();
    }

    public function fetchColumn()
    {
        return $this->sth->fetchColumn();
    }

    private function buildQuery(): string
    {
        $query = sprintf(self::SELECT, $this->select) . sprintf(self::FROM, $this->from);

        if ('' !== $this->where) {
            $query .= sprintf(self::WHERE, $this->where);
        }

        if ('' !== $this->orderBy) {
            $query .= sprintf(self::ORDER_BY, $this->orderBy);
        }

        return $query;
    }
}

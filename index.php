<?php

require_once('QueryBuilder.php');

$pdo = new PDO('mysql:host=localhost;dbname=test', 'root', 'root');

$qb = new QueryBuilder($pdo);
$result = $qb
    ->select('*')
    ->from('try')
    ->andWhere('name = ' . '"Ivan"')
    ->addOrderBy('id','DESC')
    ->execute()
    ->fetchAll();

var_dump($result);